from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.about, name='about'),
	url(r'^projects', views.projects, name='projects'),
	url(r'^contact', views.contact, name='contact'),
	url(r'^schedule', views.personal_schedule, name='schedule'),
	url(r'^delete_all_events', views.delete_all_events, name='delete_all_events')
]
