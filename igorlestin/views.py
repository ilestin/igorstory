
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import reverse
from .forms import EventForm
from .models import Event

# Create your views here.
def about(request):
	events = Event.objects.all()
	return render(request, 'about.html', {'events': events})

def projects(request):
	return render(request, 'projects.html')

def contact(request):
	return render(request, 'contact.html')

def personal_schedule(request):
	events = Event.objects.all().values()

	if request.method == "POST":
		form = EventForm(request.POST)
		if form.is_valid():
			new_event = form.save()
			return HttpResponseRedirect(reverse('schedule'))
	else:
		form = EventForm()

	response = {'form': form, 'events': events}
	return render(request, 'form.html', {'form': form, 'events': events})

def delete_all_events(request):
	events = Event.objects.all().delete()
	form = EventForm()

	return redirect('about')	
